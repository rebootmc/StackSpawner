package uk.antiperson.stackspawner;

import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;

public class Commands implements CommandExecutor
{
    private StackSpawner ss;
    private String tag;
    private String beforeBig;
    
    public Commands(final StackSpawner ss) {
        this.tag = ChatColor.DARK_PURPLE + "[" + ChatColor.AQUA + "StackSpawner" + ChatColor.DARK_PURPLE + "] ";
        this.beforeBig = ChatColor.GRAY + "------" + this.tag.replace(" ", "") + ChatColor.GRAY + "------------------------------";
        this.ss = ss;
    }
    
    public boolean onCommand(final CommandSender sender, final Command cmd, final String s, final String[] args) {
        if (sender instanceof Player) {
            final Player p = (Player)sender;
            if (args.length == 0) {
                if (p.hasPermission("StackSpawner.Commands") || p.hasPermission("StackSpawner.*")) {
                    p.sendMessage(this.beforeBig);
                    p.sendMessage(ChatColor.GOLD + "/sts about " + ChatColor.YELLOW + "Shows information about this plugin.");
                }
                else {
                    sender.sendMessage(this.tag + ChatColor.RED + "Error: You don't have the permission to do that!");
                }
            }
            else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("about")) {
                    if (p.hasPermission("StackSpawner.Commands") || p.hasPermission("StackSpawner.*")) {
                        p.sendMessage(this.beforeBig);
                        p.sendMessage(ChatColor.GREEN + "StackSpawner v" + this.ss.getDescription().getVersion() + " by antiPerson.");
                        p.sendMessage(ChatColor.GREEN + "Find out more at " + this.ss.getDescription().getWebsite());
                        p.sendMessage(ChatColor.GREEN + "Has this plugin helped you? Please leave a review, it helps a lot!");
                    }
                    else {
                        sender.sendMessage(this.tag + ChatColor.RED + "Error: You don't have the permission to do that!");
                    }
                }
                else {
                    sender.sendMessage(this.tag + ChatColor.RED + "Error: Invaild arguments!");
                }
            }
            else {
                sender.sendMessage(this.tag + ChatColor.RED + "Error: Invaild arguments!");
            }
        }
        else {
            sender.sendMessage(this.tag + ChatColor.RED + "Error: You must be a player!");
        }
        return false;
    }
}
