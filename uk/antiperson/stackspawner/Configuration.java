package uk.antiperson.stackspawner;

import java.io.IOException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.file.FileConfiguration;
import java.io.File;

public class Configuration
{
    private File file;
    private FileConfiguration filecon;
    private StackSpawner ss;
    
    public Configuration(final StackSpawner ss) {
        this.ss = ss;
        this.file = new File(ss.getDataFolder(), "config.yml");
        this.filecon = (FileConfiguration)YamlConfiguration.loadConfiguration(this.file);
    }
    
    public void generateConfig() {
        this.filecon.set("spawner.nametag.display", (Object)true);
        this.filecon.set("spawner.nametag.format", (Object)"&a%amount%x &6%type%");
        this.filecon.set("spawner.nametag.always-visible", (Object)true);
        this.filecon.set("spawner.stacking.limit", (Object)0);
        this.filecon.set("spawner.stacking.radius", (Object)5);
        try {
            this.filecon.save(this.file);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public boolean exists() {
        return this.file.exists();
    }
    
    public FileConfiguration getFilecon() {
        return this.filecon;
    }
}
