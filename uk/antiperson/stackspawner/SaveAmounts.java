package uk.antiperson.stackspawner;

import java.io.IOException;
import org.bukkit.World;
import java.util.Iterator;
import org.bukkit.Location;
import org.bukkit.Bukkit;
import java.util.UUID;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.file.FileConfiguration;
import java.io.File;

public class SaveAmounts
{
    private StackSpawner ss;
    private File file;
    private FileConfiguration filecon;
    
    public SaveAmounts(final StackSpawner ss) {
        this.ss = ss;
        this.file = new File(ss.getDataFolder(), "spawnerinfo.yml");
        this.filecon = (FileConfiguration)YamlConfiguration.loadConfiguration(this.file);
    }
    
    public void loadStacks() {
        this.ss.getLogger().info("Loading spawner stack amounts...");
        for (final String s : this.filecon.getKeys(false)) {
            final Double x = this.filecon.getDouble(s + ".x");
            final Double y = this.filecon.getDouble(s + ".y");
            final Double z = this.filecon.getDouble(s + ".z");
            final World world = Bukkit.getWorld(UUID.fromString(this.filecon.getString(s + ".world")));
            final Location loc = new Location(world, (double)x, (double)y, (double)z);
            this.ss.spawnerAmount.put(loc, this.filecon.getInt(s + ".amount"));
        }
        this.ss.getLogger().info("Loaded spawner stack amounts!");
        this.file.delete();
    }
    
    public void saveStacks() {
        this.ss.getLogger().info("Saving spawner stack amounts...");
        this.filecon.options().header("This file is for the storing of StackSpawner spawner amounts. \n There is nothing that should be changed here.");
        int i = 0;
        for (final Location loc : this.ss.spawnerAmount.keySet()) {
            ++i;
            this.filecon.set(i + ".x", (Object)loc.getX());
            this.filecon.set(i + ".y", (Object)loc.getY());
            this.filecon.set(i + ".z", (Object)loc.getZ());
            this.filecon.set(i + ".world", (Object)loc.getWorld().getUID().toString());
            this.filecon.set(i + ".amount", (Object)this.ss.spawnerAmount.get(loc));
        }
        try {
            this.filecon.save(this.file);
            this.ss.getLogger().info("Saved all spawner stack amounts!");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
