package uk.antiperson.stackspawner;

import org.bukkit.command.CommandExecutor;
import uk.antiperson.stackspawner.events.BreakEvent;
import uk.antiperson.stackspawner.events.PlaceEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.event.Listener;
import uk.antiperson.stackspawner.events.SpawnerEvent;
import java.util.HashSet;
import org.bukkit.Location;
import java.util.HashMap;
import org.bukkit.plugin.java.JavaPlugin;

public class StackSpawner extends JavaPlugin
{
    public Utilities util;
    public HashMap<Location, Integer> spawnerAmount;
    public HashSet<Location> newSpawner;
    public Configuration config;
    private SaveAmounts sa;
    
    public StackSpawner() {
        this.util = new Utilities(this);
        this.spawnerAmount = new HashMap<Location, Integer>();
        this.newSpawner = new HashSet<Location>();
        this.config = new Configuration(this);
        this.sa = new SaveAmounts(this);
    }
    
    public void onEnable() {
        this.getLogger().info("StackSpawner v" + this.getDescription().getVersion() + " by antiPerson");
        this.getLogger().info("Find more information at " + this.getDescription().getVersion());
        this.sa.loadStacks();
        if (!this.config.exists()) {
            this.getLogger().info("Generating new configuration file...");
            this.config.generateConfig();
            this.getLogger().info("Generated configuration file!");
        }
        this.getLogger().info("Registering event listeners and commands...");
        this.getServer().getPluginManager().registerEvents((Listener)new SpawnerEvent(this), (Plugin)this);
        this.getServer().getPluginManager().registerEvents((Listener)new PlaceEvent(this), (Plugin)this);
        this.getServer().getPluginManager().registerEvents((Listener)new BreakEvent(this), (Plugin)this);
        this.getCommand("sts").setExecutor((CommandExecutor)new Commands(this));
        this.getLogger().info("Regestered event listeners and commands!");
    }
    
    public void onDisable() {
        this.sa.saveStacks();
    }
}
