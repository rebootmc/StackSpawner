package uk.antiperson.stackspawner;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.Bukkit;
import org.bukkit.entity.ArmorStand;
import java.util.Iterator;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.block.Block;
import java.util.Random;

public class Utilities
{
    private StackSpawner ss;
    private Random rand;
    
    public Utilities(final StackSpawner ss) {
        this.rand = new Random();
        this.ss = ss;
    }
    
    public Random random() {
        return this.rand;
    }
    
    public List<Block> getBlocks(final Block start, final int radius) {
        final int iterations = radius * 2 + 1;
        final List<Block> blocks = new ArrayList<Block>(iterations * iterations * iterations);
        for (int x = -radius; x <= radius; ++x) {
            for (int y = -radius; y <= radius; ++y) {
                for (int z = -radius; z <= radius; ++z) {
                    blocks.add(start.getRelative(x, y, z));
                }
            }
        }
        blocks.remove(start);
        return blocks;
    }
    
    public LivingEntity getArmorStand(final Block block) {
        for (final Entity e : block.getWorld().getNearbyEntities(block.getLocation().add(0.5, 0.0, 0.5), 0.2, 0.1, 0.2)) {
            if (e.getType() == EntityType.ARMOR_STAND) {
                return (LivingEntity)e;
            }
        }
        return null;
    }
    
    public void createNewArmorstand(final Block e) {
        final ArmorStand am = (ArmorStand)e.getWorld().spawnEntity(e.getLocation().add(0.5, 0.0, 0.5), EntityType.ARMOR_STAND);
        am.setSmall(true);
        am.setVisible(false);
        am.setGravity(false);

        am.setCustomNameVisible(this.ss.config.getFilecon().getBoolean("spawner.nametag.always-visible"));
        this.ss.spawnerAmount.put(e.getLocation(), 1);
        if (this.ss.config.getFilecon().getBoolean("spawner.nametag.display")) {
            this.ss.util.updateTag(am);
        }
    }
    
    public void updateTag(final ArmorStand as) {
        final CreatureSpawner cs = (CreatureSpawner)as.getLocation().getBlock().getState();
        final String a = ChatColor.translateAlternateColorCodes('&', this.ss.config.getFilecon().getString("spawner.nametag.format"));
        final String b = a.replace("%amount%", this.ss.spawnerAmount.get(cs.getLocation()).toString()).replace("%type%", StringUtils.capitalize(cs.getCreatureTypeName().replace("_", " ")));
        as.setCustomName(b);
    }
}
