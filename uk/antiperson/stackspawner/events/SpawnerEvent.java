package uk.antiperson.stackspawner.events;

import org.bukkit.event.EventHandler;
import uk.antiperson.stackmob.api.StackMobAPI;
import uk.antiperson.stackmob.StackMob;
import org.bukkit.Bukkit;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import uk.antiperson.stackspawner.StackSpawner;
import org.bukkit.event.Listener;

public class SpawnerEvent implements Listener
{
    private StackSpawner ss;
    
    public SpawnerEvent(final StackSpawner ss) {
        this.ss = ss;
    }
    
    @EventHandler
    public void onSpawnerSpawn(final SpawnerSpawnEvent e) {
        if (this.ss.spawnerAmount.containsKey(e.getSpawner().getLocation())) {
            if (this.ss.newSpawner.contains(e.getSpawner().getLocation())) {
                this.ss.newSpawner.remove(e.getSpawner().getLocation());
                e.getSpawner().setDelay(-1);
            }
            final int amount = (int)Math.round((0.5 + this.ss.util.random().nextDouble()) * this.ss.spawnerAmount.get(e.getSpawner().getLocation()));
            if (Bukkit.getPluginManager().getPlugin("StackMob") != null && Bukkit.getPluginManager().getPlugin("StackMob").isEnabled()) {
                final StackMobAPI api = ((StackMob)Bukkit.getPluginManager().getPlugin("StackMob")).getAPI();
                api.getEntityManager().addNewStack(e.getEntity(), amount);
                ((StackMob)api.getPlugin()).noStack.add(e.getEntity().getUniqueId());
            }
            else {
                for (int i = 0; i < amount; ++i) {
                    e.getEntity().getWorld().spawnEntity(e.getLocation(), e.getEntityType());
                }
            }
        }
    }
}
