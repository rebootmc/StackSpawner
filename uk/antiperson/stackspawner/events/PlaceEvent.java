package uk.antiperson.stackspawner.events;

import org.bukkit.event.EventHandler;
import org.bukkit.plugin.Plugin;
import org.bukkit.entity.LivingEntity;
import java.util.Iterator;
import org.bukkit.entity.ArmorStand;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.Material;
import org.bukkit.event.block.BlockPlaceEvent;
import uk.antiperson.stackspawner.StackSpawner;
import org.bukkit.event.Listener;

public class PlaceEvent implements Listener
{
    private StackSpawner ss;
    
    public PlaceEvent(final StackSpawner ss) {
        this.ss = ss;
    }
    
    @EventHandler
    public void onBlockPlace(final BlockPlaceEvent e) {
        if (e.getBlock().getType() == Material.MOB_SPAWNER && e.getPlayer().hasPermission("stackspawner.stack")) {
            this.ss.getServer().getScheduler().runTaskLater((Plugin)this.ss, (Runnable)new Runnable() {
                @Override
                public void run() {
                    final CreatureSpawner first = (CreatureSpawner)e.getBlock().getState();
                    for (final Block block : PlaceEvent.this.ss.util.getBlocks(e.getBlock(), PlaceEvent.this.ss.config.getFilecon().getInt("spawner.stacking.radius"))) {
                        if (block.getType() == Material.MOB_SPAWNER) {
                            final CreatureSpawner cs = (CreatureSpawner)block.getState();
                            if (cs.getSpawnedType() != first.getSpawnedType()) {
                                continue;
                            }
                            ((CreatureSpawner)e.getBlock().getState()).setDelay(200);
                            ((CreatureSpawner)e.getBlock().getState()).update();
                            if (PlaceEvent.this.ss.spawnerAmount.containsKey(block.getLocation()) && (PlaceEvent.this.ss.spawnerAmount.get(block.getLocation()) < PlaceEvent.this.ss.config.getFilecon().getInt("spawner.stacking.max") || PlaceEvent.this.ss.config.getFilecon().getInt("spawner.stacking.max") == 0)) {
                                PlaceEvent.this.ss.spawnerAmount.put(block.getLocation(), PlaceEvent.this.ss.spawnerAmount.get(block.getLocation()) + 1);
                                if (PlaceEvent.this.ss.config.getFilecon().getBoolean("spawner.nametag.display")) {
                                    final LivingEntity le = PlaceEvent.this.ss.util.getArmorStand(block);
                                    PlaceEvent.this.ss.util.updateTag((ArmorStand)le);
                                }
                                e.getBlock().setType(Material.AIR);
                                return;
                            }
                            continue;
                        }
                    }
                    PlaceEvent.this.ss.util.createNewArmorstand(e.getBlock());
                }
            }, 1L);
        }
    }
}
