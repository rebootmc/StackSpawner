package uk.antiperson.stackspawner.events;

import de.dustplanet.util.SilkUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import uk.antiperson.stackspawner.StackSpawner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BreakEvent implements Listener
{
	private StackSpawner ss;

	public BreakEvent(final StackSpawner ss)
	{
		this.ss = ss;
	}

	@EventHandler
	public void onSpawnerBreak(final BlockBreakEvent e)
	{
		if (!e.isCancelled() && this.ss.spawnerAmount.containsKey(e.getBlock().getLocation()))
		{
			check(e.getBlock());
		}
	}

	@EventHandler
	public void onSpawnerExplode(EntityExplodeEvent event)
	{
		if (event.isCancelled())
		{
			return;
		}

		for (Block block : event.blockList())
		{
			if (block == null)
			{
				continue;
			}

			check(block);
		}
	}

	public void check(Block block) {
		check(block, false);
	}

	public void check(Block block, boolean drop)
	{
		if (!this.ss.spawnerAmount.containsKey(block.getLocation()))
		{
			return;
		}

		if (this.ss.spawnerAmount.get(block.getLocation()) > 1)
		{
			final EntityType et = ((CreatureSpawner) block.getState()).getSpawnedType();
			this.ss.getServer().getScheduler().runTaskLater((Plugin) this.ss, (Runnable) new Runnable()
			{
				@Override
				public void run()
				{
					block.setType(Material.MOB_SPAWNER);
					((CreatureSpawner) block.getState()).setSpawnedType(et);
					((CreatureSpawner) block.getState()).setDelay(200);
					((CreatureSpawner) block.getState()).update();
					BreakEvent.this.ss.spawnerAmount.put(block.getLocation(), BreakEvent.this.ss.spawnerAmount.get(block.getLocation()) - 1);
					BreakEvent.this.ss.newSpawner.add(block.getLocation());
					BreakEvent.this.ss.util.updateTag((ArmorStand) BreakEvent.this.ss.util.getArmorStand(block));

					if(drop)
					{
						SilkUtil su = SilkUtil.hookIntoSilkSpanwers();

						ItemStack item = su.newSpawnerItem(et.getTypeId(), null, 1, false);
						block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item);
					}
				}
			}, 2L);
		} else
		{
			this.ss.util.getArmorStand(block).remove();
			this.ss.spawnerAmount.remove(block.getLocation());
		}
	}

}
